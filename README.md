# arctic-data-ecosystem

Semantic files describing the Polar data organizational ecosystem and programs for visualizing the data.

## Running the server
* `npm start`

## Startup full project
Run the following commands, these will start servers that need to stay running

* `./fuseki-server`
* Go to server and
  * Add new dataset 'arcticeco'
  * Upload arctic-data-ecosystem.ttl and org.ttl
* `npm start`

## Working on the project

### Prerequisites
*  node.js/npm

### Validating the ttl files
* `npm run validatettl`

### Workspace Setup
*  Run `npm install` to pull down the project packages

### Linting
* Run `npm run lint` - NOT YET IMPLEMENTED

### Unit Testing
Not currently implemented, when it is * Run `npm test` or `npm test:watch`

### Convert Google Spreadsheet to TTL
* Generate an orgs csv: `awk -F "," '{if (NR!=1) print $1,",", $2} {if(NR!=1) print $3,",", $4}' agg_source_links.csv | sort -u | grep -v '^ ,' > orgs.csv`
* `npm run-script convertcsv`

