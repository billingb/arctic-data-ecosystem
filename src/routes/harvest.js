var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('harvest', { title: 'Arctic Data Metadata Harvesting Graph' });
});

module.exports = router;
