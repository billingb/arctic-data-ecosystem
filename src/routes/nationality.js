var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('nationality', { title: 'Arctic Data Organization Nationality Graph' });
});

module.exports = router;
