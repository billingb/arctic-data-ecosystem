var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('relationships', { title: 'Organization Relationships Graph' });
});

module.exports = router;
