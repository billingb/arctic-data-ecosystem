var width, svg, height;
var url;
var nodeColor;
var linkColor = d3.scaleOrdinal(d3.schemeCategory20b).range(['#1f78b4', '#33a02c', '#e31a1c', '#ff7f00', '#6a3d9a', '#b15928']);
var simulation;


function resize() {
  var cliWidth = document.getElementById('graphDiv').clientWidth;
  var cliHeight = $(window).height();
  width = cliWidth - 45; //take out padding/margin/etc
  height = cliHeight * .95;
  if(cliHeight > 500 && cliHeight <= 850) {
    height = cliHeight * .9;
  } else if(cliHeight > 875) {
    height = cliHeight - 100;
  }
  console.log("got height: " + height);
  d3.select('#graphDiv svg')
    .attr('width', width)
    .attr('height', height);
}

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

function addFilterOptions(nodes, links) {
  d3.select("#filterList").html(""); //Delete all items.
  d3.select("#filterList").append('option').text("").attr('value', ""); //Add top blank entry


  nodes.forEach(function(node) {
    var txt = node.label;
    if(node.description !== undefined) {
      txt = node.description + "(" + node.label + ")";
    }
    d3.select("#filterList").append('option').text(txt).attr('value', "node::" + node.label);
  });

  var legendVals = Array.from(new Set(links.map(function(link) {
    return link.legend;
  }))).sort();
  legendVals.forEach(function(val) {
    d3.select("#filterList").append('option').text(val).attr('value', "link::" + val);
  });

  var legendVals = Array.from(new Set(nodes.map(function(node) {
    return node.legend;
  }))).sort();
  legendVals.forEach(function(val) {
    d3.select("#filterList").append('option').text(val).attr('value', "nodelegend::" + val);
  });
}

function changeGraph(changeFunc, endpoint) {
  var filter = d3.select("#filterList").property("value");
  var filterParts = filter.split('::');
  var filterQuery;
  if(filterParts[0] === 'node') {
    filterQuery = baseQueries[endpoint].node.query.replace(/\[\[subjectfilter]]/g, filterParts[1]);
  } else if(filterParts[0] === 'link') {
    filterQuery = baseQueries[endpoint].link.query.replace(/\[\[linktype]]/g, filterParts[1]);
  } else if(filterParts[0] === 'nodelegend') {
    filterQuery = baseQueries[endpoint].nodelegend.query.replace(/\[\[nodetype]]/g, filterParts[1]);
  }
  changeFunc(endpoint, filterQuery);
}

function initGraph(endpoint) {
  //	filter button event handlers
  d3.select("#sparqlQuery").on("click", function() {
    changeGraph(newGraph, endpoint);
  });

//	filter button event handlers
  d3.select("#sparqlQueryFilter").on("click", function() {
    changeGraph(filterGraphByQuery, endpoint);
  });

  svg = d3.select('#graphDiv').append('svg');
  resize();

  if(endpoint === 'harvest') {
    nodeColor = d3.scaleOrdinal(d3.schemeCategory20b).range(['#000000', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c', '#fdbf6f', '#ff7f00', '#cab2d6', '#6a3d9a', '#ffff99', '#b15928']);
  } else {
    nodeColor = d3.scaleOrdinal(d3.schemeCategory20b).range(['#a6cee3', '#33a02c', '#fb9a99', '#ff7f00', '#6a3d9a', '#b15928', '#e31a1c', '#b2df8a', '#1f78b4', '#fdbf6f', '#cab2d6', '#ffff99']);
  }

  url = 'sparql/' + endpoint + '/query';
  if(window.location.href.includes('localhost')) {
    url = 'http://localhost:3030/'+ endpoint + '/query';
  }

  var collide = 35;
  var dist = 150;
  if(endpoint === 'nationality') {
    collide = 21;
    dist = 100;
  }
  if(document.getElementById('graphDiv').clientHeight < 450 || document.getElementById('graphDiv').clientWidth < 450) {
    collide = 20;
    dist = 75;
  }


  simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }))
    .force("charge", d3.forceManyBody().strength(-50))
    .force("radius", d3.forceCollide(collide).strength(.8))
    .force("charge", d3.forceManyBody().distanceMax(dist))
    .force("center", d3.forceCenter(width / 2, height / 2));
// .force('x', d3.forceX().x(function(d){ return 100*(1 - d.edges/250) }))
    // .force('y', d3.forceY().y(function(d){ return 100*(1 - d.edges/250) }))

  newGraph(endpoint);
}

function filterGraphByQuery(endpoint, query) {
  var catQuery = query;

  d3.sparql(url, catQuery).get(function(error, data) {
    if (error) throw error;

    console.log(data);

    //get object/subject ids
    var objectIds = data.map(t => t.object);
    var subjectIds = data.map(t => t.subject);
    var ids = objectIds.concat(subjectIds);
    var links = data.map(t => (t.object + ' to ' + t.subject));

    svg.selectAll('g')
      .transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style("opacity", function(n) {
        return (typeof n === 'undefined' || typeof n.id === 'undefined' || ids.includes(n.id)) ? 1 : 0.2;
      });

    svg.selectAll('path').transition()
      .duration(1000)
      .ease(d3.easeLinear)
      .style("opacity", function(l) {
        return (typeof l.source === 'undefined' || typeof l.source.id === 'undefined'
        || typeof l.target === 'undefined' || typeof l.target.id === 'undefined'
        || links.includes(l.source.id + ' to ' + l.target.id) || links.includes(l.target.id + ' to ' + l.source.id)) ? 1 : 0.2;
      })
  })
}

function newGraph(endpoint, query) {
  var catQuery = query;
  if(query === undefined) {
    catQuery = baseQueries[endpoint].all.query;
  }

  d3.sparql(url, catQuery).get(function(error, data) {
    if (error) throw error;

    console.log(data);

    svg.selectAll('*').remove();

    var graph = transformData(data, {
      "key1": 'subject',
      "key2": 'object',
      "label1": 'slabel',
      "label2": 'olabel'
    });

    addFilterOptions(graph.nodes, graph.links);

    svg.append("svg:defs").selectAll("marker")
      .data(["end"])      // Different link/path types can be defined here
      .enter().append("svg:marker")    // This section adds in the arrows
      .attr("id", String)
      .attr("viewBox", "-5 -5 10 10")
      .attr("refX", 15)
      .attr("refY", 0)
      .attr("markerWidth", 3)
      .attr("markerHeight", 3)
      .attr("orient", "auto")
      .append("svg:path")
      .attr("d", "M-5,-5L5,0L-5,5 Z");

    var link = svg.selectAll(".link")
      .data(graph.links)
      .enter().append("path")
      .attr("class", "link")
      .attr("stroke-width", 2)
      .attr("marker-end", "url(#end)")
      .attr("data-legend", function(d) { return d.legend })
      .style("opacity", .75)
      .style("stroke", function(d){
        return linkColor(d.legend);
      });

    var node = svg.selectAll(".node")
      .data(graph.nodes)
      .enter().append("g")
      .attr("country", function (n) {return n.country})
      .attr("class", "node");
    node.append("svg:circle")
      .attr("r", 5)
      .style("fill", function(d) {
        return nodeColor(d.legend);
      })
      .style("stroke", "#FFF")
      .style("stroke-width", function(d) {return d.schema === 'placename' ? 1 : 3});
    node.append("text")
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(function(d) {
        if(d.label) {
          return d.label;
        }
        return d.id;
      });
    node.call(d3.drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended));

    simulation
      .nodes(graph.nodes)
      .on("tick", ticked);

    simulation.force("link")
      .links(graph.links);

    simulation.alpha(1).restart();

    var legend = svg.append('g')
      .attr("class", "legend")
      .attr("height", 0)
      .attr("width", 0)
      .attr('transform', 'translate(20,20)');

    var legendRect = legend
      .selectAll('g')
      .data(nodeColor.domain().concat(linkColor.domain()));

    var legendRectE = legendRect.enter()
      .append("g")
      .attr("transform", function(d,i){
        return 'translate(0, ' + (i * 20) + ')';
      });

    legendRectE
      .filter(function(d) { return nodeColor.domain().includes(d); })
      .append('circle')
      .attr('r', 5)
      .attr('cx', 5)
      .style('fill', nodeColor)
      .style('stroke', nodeColor);

    legendRectE
      .filter(function(d) { return linkColor.domain().includes(d); })
      .append('rect')
      .attr('width', 10)
      .attr('height', 2)
      .style('fill', linkColor)
      .style('stroke', linkColor);

    legendRectE
      .append("text")
      .attr("x", 15)
      .attr("y", 5)
      .text(function(d) {
        if(typeof d !== 'undefined') {
          return d;
        } else {
          return "undefined label";
        }
        return d ;
      });

    function ticked() {
      var nodeRadius = 15;
      link.attr("d", function(d) {
        var dx = Math.max(nodeRadius, Math.min(width-nodeRadius, d.target.x - d.source.x)),
          dy = Math.max(nodeRadius, Math.min(height-nodeRadius, d.target.y - d.source.y)),
          dr = 150/d.linknum;  //linknum is defined above
        return "M" + Math.max(nodeRadius, Math.min(width-nodeRadius, d.source.x)) + "," +
          Math.max(nodeRadius, Math.min(height-nodeRadius, d.source.y)) + "A" + dr + "," + dr + " 0 0,1 " +
          Math.max(nodeRadius, Math.min(width-nodeRadius, d.target.x)) + "," +
          Math.max(nodeRadius, Math.min(height-nodeRadius, d.target.y));
      });

      node.attr("transform", function(d) { return "translate(" + Math.max(nodeRadius, Math.min(width-nodeRadius, d.x)) + "," +  Math.max(nodeRadius, Math.min(height-nodeRadius, d.y)) + ")"; });
    }
  });
}
