var baseQueries = {
  harvest: {
    all: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
    PREFIX org: <http://www.w3.org/ns/org#>\
    PREFIX pdps: <http://example.org/pdes#>\
  \
    SELECT ?subject ?object ?sdesc ?odesc ?slabel ?olabel ?linkType ?linkComment ?sNodeType ?oNodeType\
      WHERE {\
      ?subject pdps:hasHarvester ?harvestObject .\
      ?harvestObject pdps:harvestFrom ?object .\
      ?subject rdfs:label ?slabel .\
      ?object rdfs:label ?olabel .\
      ?subject rdfs:comment ?sdesc .\
      ?object rdfs:comment ?odesc .\
      ?harvestObject pdps:harvestStatus ?linkType .\
      OPTIONAL {?harvestObject rdfs:comment ?linkComment } .\
      VALUES ?sNodeType { "Organization" }.\
      VALUES ?oNodeType { "Organization" }.\
      }',
      label: 'All'
    },
    node: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
    PREFIX pdps: <http://example.org/pdes#>\
    SELECT ?subject ?object ?slabel ?olabel ?linkType ?linkComment ?sNodeType ?oNodeType\
      WHERE {\
      ?subject pdps:hasHarvester ?harvestObject .\
      ?harvestObject pdps:harvestFrom ?object .\
      ?subject rdfs:label ?slabel .\
      ?object rdfs:label ?olabel .\
      ?harvestObject pdps:harvestStatus ?linkType\
      OPTIONAL {?harvestObject rdfs:comment ?linkComment } .\
      VALUES ?sNodeType { "Organization" }.\
      VALUES ?oNodeType { "Organization" }.\
  FILTER (?slabel = "[[subjectfilter]]" || ?olabel = "[[subjectfilter]]").\
      }',
      label: 'Organization'
    },
    link: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
    PREFIX pdps: <http://example.org/pdes#>\
    SELECT ?subject ?object ?slabel ?olabel ?linkType ?linkComment ?sNodeType ?oNodeType\
      WHERE {\
      ?subject pdps:hasHarvester ?harvestObject .\
      ?harvestObject pdps:harvestFrom ?object .\
      ?subject rdfs:label ?slabel .\
      ?object rdfs:label ?olabel .\
      ?harvestObject pdps:harvestStatus ?linkType\
      OPTIONAL {?harvestObject rdfs:comment ?linkComment } .\
      VALUES ?sNodeType { "Organization" }.\
      VALUES ?oNodeType { "Organization" }.\
      FILTER (?linkType = "[[linktype]]").\
      }',
      label: 'Link Type'
    }
  },
  relationships: {
    all: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdesclassification: <http://example.org/pdes-organization-classifications#>\
  PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>\
  PREFIX pdes: <http://example.org/pdes#>\
      SELECT ?subject ?object ?slabel ?olabel ?linkType ?scountry ?ocountry ?sNodeType ?oNodeType\
      WHERE {\
        ?subject ?linkTypePred ?object .\
        ?subject rdfs:label ?slabel .\
        ?object rdfs:label ?olabel .\
        OPTIONAL {?subject org:classification ?sClassification .\
        ?sClassification rdfs:label ?sNodeType } .\
      OPTIONAL {?object org:classification ?oClassification .\
  ?oClassification rdfs:label ?oNodeType } .\
OPTIONAL {?linkTypePred rdfs:label ?linkType .\
FILTER (lang(?linkType) = "en")\
}\
?subject org:hasPrimarySite ?site .\
  ?site org:siteAddress ?siteAddr .\
  ?siteAddr vcard:country-name ?scountry .\
  ?object org:hasPrimarySite ?osite .\
  ?osite org:siteAddress ?ositeAddr .\
  ?ositeAddr vcard:country-name ?ocountry .\
}'
    },
    node: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdesclassification: <http://example.org/pdes-organization-classifications#>\
  PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>\
  PREFIX pdes: <http://example.org/pdes#>\
      SELECT ?subject ?object ?slabel ?olabel ?linkType ?scountry ?ocountry ?sNodeType ?oNodeType\
      WHERE {\
        ?subject ?linkTypePred ?object .\
        ?subject rdfs:label ?slabel .\
        ?object rdfs:label ?olabel .\
        OPTIONAL {?subject org:classification ?sClassification .\
        ?sClassification rdfs:label ?sNodeType } .\
      OPTIONAL {?object org:classification ?oClassification .\
  ?oClassification rdfs:label ?oNodeType } .\
OPTIONAL {?linkTypePred rdfs:label ?linkType .\
FILTER (lang(?linkType) = "en")\
}\
?subject org:hasPrimarySite ?site .\
  ?site org:siteAddress ?siteAddr .\
  ?siteAddr vcard:country-name ?scountry .\
  ?object org:hasPrimarySite ?osite .\
  ?osite org:siteAddress ?ositeAddr .\
  ?ositeAddr vcard:country-name ?ocountry .\
  FILTER (?slabel = "[[subjectfilter]]" || ?olabel = "[[subjectfilter]]").\
}'
    },
    nodelegend: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdesclassification: <http://example.org/pdes-organization-classifications#>\
  PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>\
  PREFIX pdes: <http://example.org/pdes#>\
      SELECT ?subject ?object ?slabel ?olabel ?linkType ?scountry ?ocountry ?sNodeType ?oNodeType\
      WHERE {\
        ?subject ?linkTypePred ?object .\
        ?subject rdfs:label ?slabel .\
        ?object rdfs:label ?olabel .\
        OPTIONAL {?subject org:classification ?sClassification .\
        ?sClassification rdfs:label ?sNodeType } .\
      OPTIONAL {?object org:classification ?oClassification .\
  ?oClassification rdfs:label ?oNodeType } .\
OPTIONAL {?linkTypePred rdfs:label ?linkType .\
FILTER (lang(?linkType) = "en")\
}\
?subject org:hasPrimarySite ?site .\
  ?site org:siteAddress ?siteAddr .\
  ?siteAddr vcard:country-name ?scountry .\
  ?object org:hasPrimarySite ?osite .\
  ?osite org:siteAddress ?ositeAddr .\
  ?ositeAddr vcard:country-name ?ocountry .\
  FILTER (?sNodeType = "[[nodetype]]"@en || ?oNodeType = "[[nodetype]]"@en).\
}'
    },
    link: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdesclassification: <http://example.org/pdes-organization-classifications#>\
  PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>\
  PREFIX pdes: <http://example.org/pdes#>\
      SELECT ?subject ?object ?slabel ?olabel ?linkType ?scountry ?ocountry ?sNodeType ?oNodeType\
      WHERE {\
        ?subject ?linkTypePred ?object .\
        ?subject rdfs:label ?slabel .\
  ?object rdfs:label ?olabel .\
  ?subject org:classification ?sClassification .\
  ?sClassification rdfs:label ?sNodeType .\
  ?object org:classification ?oClassification .\
  ?oClassification rdfs:label ?oNodeType .\
  ?linkTypePred rdfs:label ?linkType .\
  FILTER (?linkType = "[[linktype]]"@en) .\
  ?subject org:hasPrimarySite ?site .\
  ?site org:siteAddress ?siteAddr .\
  ?siteAddr vcard:country-name ?scountry .\
  ?object org:hasPrimarySite ?osite .\
  ?osite org:siteAddress ?ositeAddr .\
  ?ositeAddr vcard:country-name ?ocountry .\
  }'
    }
  },
  nationality: {
    all: {
      query: '  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdps: <http://example.org/pdps#>\
  SELECT ?subject ?object ?sdesc ?odesc ?slabel ?olabel ?linkType ?sNodeType ?oNodeType\
    WHERE {\
        ?subject pdps:nationality ?object .\
  ?object rdfs:label ?olabel .\
  ?subject rdfs:label ?slabel .\
  		?subject rdfs:type ?sNodeTypeClass .\
        ?object rdfs:type ?oNodeTypeClass .\
  ?sNodeTypeClass rdfs:label ?sNodeType .\
  ?oNodeTypeClass rdfs:label ?oNodeType .\
  VALUES ?linkType { "Nationality" }.\
FILTER (lang(?sNodeType) = "en") .\
FILTER (lang(?oNodeType) = "en") .\
}'
    },
    node: {
      query: '  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdps: <http://example.org/pdps#>\
  SELECT ?subject ?object ?slabel ?olabel ?linkType ?sNodeType ?oNodeType\
    WHERE {\
        ?subject pdps:nationality ?object .\
  ?object rdfs:label ?olabel .\
  ?subject rdfs:label ?slabel .\
  		?subject rdfs:type ?sNodeTypeClass .\
        ?object rdfs:type ?oNodeTypeClass .\
  ?sNodeTypeClass rdfs:label ?sNodeType .\
  ?oNodeTypeClass rdfs:label ?oNodeType .\
  VALUES ?linkType { "Nationality" }.\
FILTER (lang(?sNodeType) = "en") .\
FILTER (lang(?oNodeType) = "en") .\
FILTER (?slabel = "[[subjectfilter]]" || ?olabel = "[[subjectfilter]]").\
}'
    },
    nodelegend: {
      query: '  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdps: <http://example.org/pdps#>\
  SELECT ?subject ?object ?slabel ?olabel ?linkType ?sNodeType ?oNodeType\
    WHERE {\
        ?subject pdps:nationality ?object .\
  ?object rdfs:label ?olabel .\
  ?subject rdfs:label ?slabel .\
  		?subject rdfs:type ?sNodeTypeClass .\
        ?object rdfs:type ?oNodeTypeClass .\
  ?sNodeTypeClass rdfs:label ?sNodeType .\
  ?oNodeTypeClass rdfs:label ?oNodeType .\
  VALUES ?linkType { "Nationality" }.\
FILTER (lang(?sNodeType) = "en") .\
FILTER (lang(?oNodeType) = "en") .\
FILTER (?sNodeType = "[[nodetype]]"@en || ?oNodeType = "[[nodetype]]"@en).\
}'
    },
    link: {
      query: '  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
  PREFIX pdps: <http://example.org/pdps#>\
  SELECT ?subject ?object ?slabel ?olabel ?linkType ?sNodeType ?oNodeType\
    WHERE {\
        ?subject ?linkTypePred ?object .\
        ?linkTypePred rdfs:label ?linkType .\
  ?object rdfs:label ?olabel .\
  ?subject rdfs:label ?slabel .\
  		?subject rdfs:type ?sNodeTypeClass .\
        ?object rdfs:type ?oNodeTypeClass .\
  ?sNodeTypeClass rdfs:label ?sNodeType .\
  ?oNodeTypeClass rdfs:label ?oNodeType .\
FILTER (lang(?sNodeType) = "en") .\
FILTER (lang(?oNodeType) = "en") .\
FILTER (?linkType = "[[linktype]]"@en) .\
}'
    }
  }
};
