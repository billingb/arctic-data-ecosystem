function transformData(json, config) {
  config = config || {};

  var head = Object.keys(json[0]);
  var data = json;

  var opts = {
    "key1":   config.key1   || head[0] || "key1",
    "key2":   config.key2   || head[1] || "key2",
    "label1": config.label1 || head[2] || false,
    "label2": config.label2 || head[3] || false,
    "value1": config.value1 || head[4] || false,
    "value2": config.value2 || head[5] || false,
  };

  var graph = {
    "nodes": [],
    "links": []
  };

  var check = d3.map();
  var nodeCount = {};
  var index = 0;
  for (var i = 0; i < data.length; i++) {
    var key1 = data[i][opts.key1];
    var key2 = data[i][opts.key2];
    var label1 = opts.label1 ? data[i][opts.label1] : key1;
    var label2 = opts.label2 ? data[i][opts.label2] : key2;
    var value1 = opts.value1 ? data[i][opts.value1] : false;
    var value2 = opts.value2 ? data[i][opts.value2] : false;

    if (!check.has(key1)) {
      graph.nodes.push({id: key1, "key": key1, "label": label1, "value": value1, legend: data[i].sNodeType,
        description: data[i].sdesc,
        "country": data[i].scountry, classification: data[i].sClassificationName});
      check.set(key1, index);
      index++;
    }
    if (!check.has(key2)) {
      graph.nodes.push({id: key2, "key": key2, "label": label2, "value": value2, legend: data[i].oNodeType,
        "country": data[i].ocountry,
        description: data[i].odesc,
        classification: data[i].oClassificationName});
      check.set(key2, index);
      index++;
    }
    graph.links.push({"source": key1, "target": key2, legend: data[i].linkType});
    if(typeof nodeCount[key1] !== 'undefined') {
      nodeCount[key1]++;
    } else {
      nodeCount[key1] = 0;
    }
    if(typeof nodeCount[key2] !== 'undefined') {
      nodeCount[key2]++;
    } else {
      nodeCount[key2] = 0;
    }
  }
  for(var i = 0; i < graph.nodes.length; i++) {
    graph.nodes[i].edges = nodeCount[graph.nodes[i].id];
  }

  //Following code from http://jsfiddle.net/7HZcR/3/
  //sort links by source, then target
  graph.links.sort(function(a,b) {
    if (a.source > b.source) {return 1;}
    else if (a.source < b.source) {return -1;}
    else {
      if (a.target > b.target) {return 1;}
      if (a.target < b.target) {return -1;}
      else {return 0;}
    }
  });

  //any links with duplicate source and target get an incremented 'linknum'
  for (var i=0; i<graph.links.length; i++) {
    if (i != 0 &&
      graph.links[i].source == graph.links[i-1].source &&
      graph.links[i].target == graph.links[i-1].target) {
      graph.links[i].linknum = graph.links[i-1].linknum + 1;
    }
    else {graph.links[i].linknum = 1;};
  }

  //sort the nodes and links
  graph.nodes.sort(function (n1, n2) {
    if(n1.label.toLowerCase() < n2.label.toLowerCase()) return -1;
    if(n1.label.toLowerCase() > n2.label.toLowerCase()) return 1;
    return 0;
  });

  return graph;
}