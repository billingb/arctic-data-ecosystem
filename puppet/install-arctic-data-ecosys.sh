#!/usr/bin/env bash

sudo ln -s `which nodejs` /usr/local/bin/node

mkdir -p /vagrant/run/logs/

cd /vagrant
npm install
sudo npm install -g forever
npm rebuild node-sass
sudo update-rc.d arctic-data-ecosys defaults