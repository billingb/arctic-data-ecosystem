#!/usr/bin/env bash

wget http://mirrors.ibiblio.org/apache/jena/binaries/apache-jena-fuseki-3.13.1.tar.gz
tar xzf apache-jena-fuseki-3.13.1.tar.gz
ln -s apache-jena-fuseki-3.13.1 fuseki
cd fuseki

sudo mkdir -p /etc/fuseki
sudo cp /home/vagrant/fuseki/fuseki /etc/init.d/
sudo update-rc.d fuseki defaults