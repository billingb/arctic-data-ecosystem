# Load modules and classes
lookup('classes', {merge => unique}).include

# Need jq to parse JSON
package { 'default-jre':
  ensure => present,
  notify => File['/etc/default/fuseki']
}

package { 'nginx':
  ensure => present,
  notify => File['/etc/nginx/conf.d/nginx.conf']
}

package { 'nodejs':
  ensure => present,
  notify => Package['npm']
}

package { 'npm':
  ensure => present,
  subscribe => Package['nodejs'],
  notify => File['/etc/init.d/arctic-data-ecosys']
}

file { '/etc/default/fuseki':
  ensure  => present,
  owner   => 'root',
  group   => 'root',
  mode    => '0644',
  source  => '/vagrant/files/fuseki',
  require => Package['default-jre']
}

file { '/etc/init.d/arctic-data-ecosys':
  ensure  => present,
  owner   => 'root',
  group   => 'root',
  mode    => '0755',
  source  => '/vagrant/files/arctic-data-ecosys',
  require => Package['npm']
}

file { '/etc/nginx/sites-enabled/default':
  ensure  => absent,
  require => Package['nginx'],
  notify  => Service['nginx']
}

file { '/etc/nginx/conf.d/nginx.conf':
  ensure  => present,
  owner   => 'root',
  group   => 'root',
  mode    => '0644',
  source  => '/vagrant/files/nginx.conf',
  require => Package['nginx'],
  notify  => Service['nginx']
}

service { 'nginx':
  ensure  => running,
  enable  => true,
  require => File['/etc/nginx/sites-enabled/default']
}

exec { '/vagrant/puppet/install-fuseki.sh':
  command => '/vagrant/puppet/install-fuseki.sh',
  cwd     => '/home/vagrant',
  require => Package['default-jre']
}

exec { '/vagrant/puppet/install-arctic-data-ecosys.sh':
  command => '/vagrant/puppet/install-arctic-data-ecosys.sh',
  cwd     => '/vagrant',
  require => Package['npm']
}

service { 'fuseki':
  ensure  => running,
  enable  => true,
  require => Exec['/vagrant/puppet/install-fuseki.sh']
}

service { 'arctic-data-ecosys':
  ensure  => running,
  enable  => true,
  require => Exec['/vagrant/puppet/install-arctic-data-ecosys.sh']
}
