const fs = require('fs');
const csv = require("fast-csv");
const uuidv4 = require('uuid/v4');

const csvOrgsToList = () => {
  var orgs = {};
  return new Promise((resolve, reject) => {
  csv.parseFile("orgs.csv", {headers : ["name", "link"], trim : true})
    .on("error", err => {
      console.error('Could not read from the CSV file: org.csv');
      reject(err);
    })
    .on("data", data => {
      var newData = {};
      newData.link = data.link;
      if(data.name.includes("(")) {
        newData.label = data.name.substring(data.name.indexOf('(') + 1, data.name.indexOf(')'));
        newData.comment = data.name.substr(0, data.name.indexOf('('));
      } else {
        newData.label = data.name;
        newData.comment = data.name;
      }
      newData.fullname = data.name;
      newData.uuid = uuidv4();
      newData.harvestingFrom = [];
      orgs[data.name] = newData;
    })
    .on("end", () => resolve(orgs));
  });
};

const orgsWithHarvesting = (orgs) => {
  return new Promise((resolve, reject) => {
    csv.parseFile("agg_source_links.csv", {renameHeaders: true, headers : ["agg", "agglink", "src", "srclink", "harveststatus", "notes"], trim : true})
      .on("error", err => {
        console.error('Could not read from the CSV file: org.csv');
        reject(err);
      })
      .on("data", data => {
        var orgData = orgs[data.agg];
        if(orgData !== undefined) {
          orgData.harvestingFrom.push({source: orgs[data.src].uuid, status: data.harveststatus, notes: data.notes});
        }
      })
      .on("end", () => resolve(orgs));
  });
};

const orgToTtlString = (org) => {
  let output = '<' + org.uuid + '>\n' +
    '  rdfs:label "' + org.label + '";\n' +
    '  rdfs:comment "' + org.comment + '";\n' +
    '  foaf:homepage <' + org.link + '>;\n' +
    org.harvestingFrom.map(src => {return "  pdps:hasHarvester <" + org.uuid + '.' + src.source + '>'; }).join(";\n") +
    '.\n\n';

  output = output + org.harvestingFrom.map(src => {
      return '<' + org.uuid + '.' + src.source + '>' +
        '  pdps:harvestFrom <' + src.source + '>;\n' +
        '  pdps:harvestStatus "' + src.status + '";\n' +
        '  rdfs:comment "' + src.notes + '".\n\n'
    }).join('');

  return output;
};

csvOrgsToList().then(orgsWithHarvesting).then((orgs) => {
  var ttlOutput = "@prefix rdfs:               <http://www.w3.org/2000/01/rdf-schema#> .\n" +
    "@prefix foaf:               <http://xmlns.com/foaf/0.1/> .\n" +
    "@prefix pdps:               <http://example.org/pdes#>.\n" +
    "@prefix org:                <http://www.w3.org/ns/org#> .\n\n";

  ttlOutput = ttlOutput + Object.values(orgs).map(orgToTtlString).join('');

  fs.writeFile('aggSrc.ttl', ttlOutput, (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log('Saved ttl file!');
  });
});