const fs = require('fs');
const csv = require("fast-csv");
const uuidv4 = require('uuid/v4');

const csvOrgsToList = () => {
  var orgs = {};
  var countries = [];
  return new Promise((resolve, reject) => {
  csv.parseFile("nationalData.csv", {headers : ["name", "desc", "country", "link"], renameHeaders: true, trim : true})
    .on("error", err => {
      console.error('Could not read from the CSV file: nationalData.csv');
      reject(err);
    })
    .on("data", data => {
      var newData = {};
      newData.link = data.link;
      if(data.name.includes("(")) {
        newData.label = data.name.substring(data.name.indexOf('(') + 1, data.name.indexOf(')'));
      } else {
        newData.label = data.name;
      }
      newData.comment = data.name + ': ' + data.desc.replace(/(\r\n|\n|\r)/gm," ").replace(/"/g, '\\"');
      newData.uuid = uuidv4();
      newData.countries = data.country.split(', ');
      orgs[data.name] = newData;
      countries = countries.concat(data.country.split(', '));
    })
    .on("end", () => resolve({orgs: orgs, countries: countries}));
  });
};

const orgToTtlString = (org) => {
  let output = '<' + org.uuid + '>\n' +
    '  rdfs:type org:Organization;\n' +
    '  rdfs:label "' + org.label + '";\n' +
    '  rdfs:comment "' + org.comment + '";\n' +
    '  foaf:homepage <' + org.link + '>;\n' +
    org.countries.map(c => {return '  pdps:nationality <' + c.replace(/ /g, "-") +'>'; }).join(";\n") +
    '.\n\n';

  return output;
};

const countiresToTtlString = (c) => {
  let output = '<' + c.replace(/ /g, "-") + '>\n' +
    '  rdfs:type pdps:Country;\n' +
    '  rdfs:label "' + c + '"';
  if(c !== 'International') {
    output = output + ';\n  pdps:nationality <International>.\n\n';
  } else {
    output = output + '.\n\n';
  }

  return output;
};

const unique = (value, index, self) => {
  return self.indexOf(value.toString()) === index;
};

csvOrgsToList().then((data) => {
  var ttlOutput = "@prefix rdfs:               <http://www.w3.org/2000/01/rdf-schema#> .\n" +
    "@prefix foaf:               <http://xmlns.com/foaf/0.1/> .\n" +
    "@prefix pdps:               <http://example.org/pdps#>.\n" +
    "@prefix org:                <http://www.w3.org/ns/org#> .\n\n";

  ttlOutput = ttlOutput + Object.values(data.orgs).map(orgToTtlString).join('');
  var countries = data.countries.filter(unique);

  ttlOutput = ttlOutput + countries.map(countiresToTtlString).join('');

  fs.writeFile('nationalOrgs.ttl', ttlOutput, (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log('Saved ttl file!');
  });
});